import { createHashRouter } from 'react-router-dom';

import Home from '../pages/home'
import Test from '../pages/test'
import Scroll from '../pages/useScroll'
import UseNextPage from '../pages/useNextPage'
import UseContext from '../pages/useContext'
import UseWaterfalls from '../pages/useWaterfalls'
import ZhouYi from '../pages/zhouyi';
import Calculator from '../pages/calculator';

export const router = createHashRouter([
	{ path: '/test', element: <Test /> },
	{ path: '/scroll', element: <Scroll /> },
	{ path: '/nextPage', element: <UseNextPage /> },
	{ path: '/useContext', element: <UseContext /> },
	{ path: '/waterFalls', element: <UseWaterfalls /> },
	{ path: '/zhouyi', element: <ZhouYi /> },
	{ path: '/calculator', element: <Calculator /> },
	{ path: '/*', element: <Home /> },
]);