import React, { useContext } from "react";
import Context from '../hooks/useContext';

function ContextCnt(props) {
	const { cnt, setCnt, } = useContext(Context);
	return <div>
		Content：{cnt}
		<button onClick={() => {
			setCnt(pre => pre + 1);
		}}>+1</button>
	</div>
}

export default ContextCnt;