import React, { useEffect, useState } from 'react';
import './waterFall.css';

function WaterFalls(props) {

	const { items, column = 3, } = props;

	// const [columns, setColumns] = useState(3);
	const [columnItems, setColumnItems] = useState([]);

	useEffect(() => {
		setColumnItems([]);
		// 将items分为n列，n为columns的值
		const itemsArr = Array.from(items);
		const itemsPerColumn = Math.ceil(itemsArr.length / column);

		for (let i = 0; i < column; i++) {
			setColumnItems(pre => [
				...pre,
				itemsArr.splice(0, itemsPerColumn),
			])
		}
	}, [column, items])

	return (
		<div className='waterfall'>
			{
				columnItems.map((column, index) => (
					<div key={index} className='waterfall-column'>
						{
							column.map((item, index) => (
								<div key={`item-${index}`} className='waterfall-item'>
									<img src={item.image} alt={item.title} />
									<p>{item.title}</p>
								</div>
							))
						}
					</div>
				))
			}
		</div>
	);
}

export default WaterFalls;