import './App.css';
import { useRef } from 'react';

const list = [
  { name: 'ake', age: 24 },
  { name: 'mera', age: 24 },
  { name: 'akane', age: 24 },
];


function App() {
  const ref1 = useRef();
  const ref2 = useRef();

  // 这里初始化给{}，即 refList.current = {}
  const refList = useRef({});

  return (
    <div>
      <div ref={ref1} style={{ color: 'green' }}>Ref1</div>
      <span ref={(ele) => {
        ref2.current = ele;
      }} style={{ color: 'red' }}>Ref2</span>


      {
        list.map(item => (
          <div ref={(ele) => {
            refList.current[item.name] = ele;
          }}>refList-{item.name}-{item.age}</div>
        ))
      }


      <div className='btn'>
        <button onClick={() => {
          console.log('ref1----', ref1);
          console.log('ref2----', ref2);
          console.log('refList----', refList);
        }}>log</button>
      </div>

    </div>
  );
}

export default App;



