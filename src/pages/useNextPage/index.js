import React from 'react';
import UesNextPage from '../../hooks/useNextPage'

const NextPage = (props) => {
	const { total, list, setPageNum, } = UesNextPage();

	return (
		<div>
			{
				list.map((item, index) => (
					<div key={index}>{item}</div>
				))
			}
			{
				total > list?.length && (
					<button onClick={() => { setPageNum(pre => pre + 1) }}>查看更多</button>
				)
			}

		</div>
	);
}

export default NextPage;