import React, { useState, useEffect, } from 'react';
import WaterFalls from '../../compoents/waterFalls';
import './index.css'

const arr = [
	{ title: 'ssss1', image: 'https://img.alicdn.com/imgextra/i1/O1CN01RCGSBf1pnXYfC2Eri_!!6000000005405-2-tps-3480-1334.png' },
	{ title: 'ssss2', image: 'https://img.alicdn.com/imgextra/i3/O1CN01FkxoCx1FJvlbWU6EY_!!6000000000467-0-tps-1125-2340.jpg' },
	{ title: 'ssss3', image: 'https://img.alicdn.com/imgextra/i2/O1CN01S1psGl262E6DEGBof_!!6000000007603-2-tps-32-32.png' },
	{ title: 'ssss4', image: 'https://img.alicdn.com/imgextra/i3/O1CN01tuSkwe1Tpu2ERfBQ4_!!6000000002432-2-tps-329-316.png' },
	{ title: 'ssss5', image: 'https://img.alicdn.com/imgextra/i3/O1CN01tuSkwe1Tpu2ERfBQ4_!!6000000002432-2-tps-329-316.png' },
	{ title: 'ssss6', image: 'https://img.alicdn.com/imgextra/i3/O1CN01FkxoCx1FJvlbWU6EY_!!6000000000467-0-tps-1125-2340.jpg' },
	{ title: 'ssss7', image: 'https://img.alicdn.com/imgextra/i2/O1CN01S1psGl262E6DEGBof_!!6000000007603-2-tps-32-32.png' },
	{ title: 'ssss8', image: 'https://img.alicdn.com/imgextra/i3/O1CN01tuSkwe1Tpu2ERfBQ4_!!6000000002432-2-tps-329-316.png' },
	{ title: 'ssss9', image: 'https://img.alicdn.com/imgextra/i3/O1CN01FkxoCx1FJvlbWU6EY_!!6000000000467-0-tps-1125-2340.jpg' },
	{ title: 'ssss10', image: 'https://img.alicdn.com/imgextra/i3/O1CN01tuSkwe1Tpu2ERfBQ4_!!6000000002432-2-tps-329-316.png' },
	{ title: 'ssss11', image: 'https://img.alicdn.com/imgextra/i1/O1CN01RCGSBf1pnXYfC2Eri_!!6000000005405-2-tps-3480-1334.png' },
]

function UseWaterfalls(props) {
	return (
		<div>
			<WaterFalls items={arr} column={4}/>
		</div>
	);
}

export default UseWaterfalls;