import React, { useRef, useState, useReducer, useEffect, } from 'react';
import { Button } from 'antd';

function reducer(state, action) {
	switch (action.type) {
		case 'start':
			return { ...state, isRunning: true };
		case 'stop':
			return { ...state, isRunning: false };
		case 'reset':
			return { cnt: 0, isRunning: false };
		case 'tick':
			return { ...state, cnt: state.cnt + 1 };

		default:
			throw new Error('no such action type');
	}
}

const Test = () => {
	const [cnt, setCnt] = useState(0);

	const [state, dispatch] = useReducer(reducer, { cnt: 0, isRunning: false });

	const cntRef = useRef(cnt);

	cntRef.current = cnt;

	const idRef = useRef(0);

	// const [dom, setDom] = useState(null);
	// console.log('RefDomTest', dom);

	useEffect(() => {
		if (!state.isRunning) {
			return;
		}
		idRef.current = setInterval(() => dispatch({ type: 'tick' }), 1000);
		return () => {
			clearInterval(idRef.current);
			idRef.current = 0;
		};
	}, [state.isRunning]);


	return (
		<div>
			Test
			<div>cnt----{cnt}</div>
			<div>ref----{cntRef.current}</div>
			<div>reducer---{state.cnt}</div>
			<button onClick={() => { setCnt(pre => pre + 1) }}>+1</button>

			<button onClick={() => dispatch({ type: 'start' })}>
				Start
			</button>
			<button onClick={() => dispatch({ type: 'stop' })}>
				Stop
			</button>
			<button onClick={() => dispatch({ type: 'reset' })}>
				Reset
			</button>
			<hr />

			{/* <div ref={setDom}>RefDomTest --- {cnt}</div> */}
			<div>
				localStorage
				<Button type='primary' onClick={() => {
					console.log('change localStorage');
					localStorage.setItem('time',new Date().valueOf());
				}}>change localStorage</Button>
			</div>
		</div>
	);
};

export default Test;