import React, { useEffect } from 'react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';
import './index.css';


const Home = () => {

	useEffect(() => {
		window.onstorage = function (e) {
			console.log('home', e);
			console.log('window.localStorage is ', window.localStorage);
		}
	}, [])

	return (
		<div>
			Home
			<Link className='link' to={'/test'}><Button type='primary'>test</Button></Link>
			<Link className='link' to={'/scroll'}><Button type='primary'>useScroll</Button></Link>
			<Link className='link' to={'/nextPage'}><Button type='primary'>nextPage</Button></Link>
			<Link className='link' to={'/useContext'}><Button type='primary'>useContext</Button></Link>
			<Link className='link' to={'/waterFalls'}><Button type='primary'>waterFalls</Button></Link>
			<Link className='link' to={'/zhouyi'}><Button type='primary'>ZhouYi</Button></Link>
			<Link className='link' to={'/calculator'}><Button type='primary'>calculator</Button></Link>
		</div>
	);
};

export default Home;