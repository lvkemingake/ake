import React from 'react';
import cx from 'classnames';
import { motion } from 'framer-motion';
import UseScroll from '../../hooks/useScroll.js';
import './index.css';

const tabList = [
	{ label: '标签1', value: 'tab1', },
	{ label: '标签2', value: 'tab2', },
	{ label: '标签3', value: 'tab3', },
	{ label: '标签4', value: 'tab4', },
	{ label: '标签5', value: 'tab5', },
	{ label: '标签6', value: 'tab6', },
]

const scrollId = 'scroll-content';

const scroll = (props) => {

	const { curTab, setCurTab, dataInView, setDataInView, } = UseScroll({ list: tabList, });

	return (
		<div>
			<div className='scroll-tab-wapper'>
				<div className='scroll-tab'>
					{
						tabList.map(tab => (
							<div
								key={tab.value}
								className={cx('scroll-tab-item', curTab === tab.value && 'active')}
								onClick={() => {
									if (tab.value) {
										const ele = document.getElementById(tab.value);
										setCurTab(tab.value)
										// const { offsetTop, clientHeight, } = ele;
										// console.log('ele', ele);
										// console.log('offsetTop - clientHeight - 65', offsetTop - clientHeight - 65);

										// offsetParent 是一个只读属性，返回一个指向最近的（指包含层级上的最近）包含该元素的定位元素或者最近的 table, td, th, body 元素

										// clientHeight 它是元素内部的高度（以像素为单位），包含内边距，但不包括边框、外边距和水平滚动条
										// offsetTop 它返回当前元素相对于其 offsetParent 元素的顶部内边距的距离

										ele.scrollIntoView();

										// ele.scrollTo({ top: 65 });
									}
								}}
							>
								{tab.label}
							</div>
						))
					}
				</div>
			</div>
			<div id={scrollId} className='scroll-content'>
				{
					tabList.map((item, index) => (
						<motion.div
							id={item.value}
							key={item.label}
							className='scroll-item'
							style={{
								height: 200 + index * 100,
							}}
							onViewportEnter={() => {
								// 元素进入到可视窗口

								// 防止重复元素
								if (dataInView.includes(index)) {
									return
								}

								setDataInView(pre => {
									const newData = pre.concat(index);
									// 总是先选中最上面的tab，所以滑动中每次选中的tab下标肯定是最小的
									const minIndex = Math.min(...newData);
									const curTab = tabList?.[minIndex];
									setCurTab(curTab.value);
									return newData;
								})
							}}
							onViewportLeave={() => {
								// 元素离开可视窗口
								setDataInView(pre => {
									const newData = pre.filter(item => item !== index);
									const minIndex = Math.min(...newData);
									const curTab = tabList?.[minIndex];
									setCurTab(curTab.value);

									return newData;
								})
							}}
						>
							{item.value + ' ---- ' + item.label}
						</motion.div>
					))
				}
			</div>
		</div>
	);
}

export default scroll;