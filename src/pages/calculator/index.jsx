/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import './index.css';


// '=', 'Backspace'
const operatorList = [
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
	'+', '-', '*', '/', '%', '.'
];
const numList = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
const operator = ['+', '-', '*', '/', '%'];

function Calculator(props) {

	const [calculatorStr, setCalculatorStr] = useState('');
	const [res, setRes] = useState(null);
	const [curHasPoint, setCurHasPoint] = useState(false);

	useEffect(() => {
		document.onkeyup = (e) => {
			if (operatorList.includes(e.key)) {
				setCalculatorStr((oldVal) => {
					return oldVal + e.key;
				})
			}
		}
	}, []);

	const operatorAction = (keyCode, type) => {
		let end = calculatorStr.slice(-1, calculatorStr.length);
		let addStr = '';
		switch (type) {
			// 数字
			case 'num':
				setCalculatorStr((oldVal) => {
					return oldVal + keyCode;
				})
				break;
			// 小数点
			case 'point':
				if (['', ...operator].includes(end)) {
					addStr = '0.';
				} else if (numList.includes(end) && !curHasPoint) {
					addStr = '.';
				}
				setCalculatorStr((oldVal) => {
					return oldVal + addStr;
				})
				setCurHasPoint(true);
				break;
			case 'operator':
				if (!['', '.', ...operator].includes(end)) {
					setCalculatorStr(oldVal => oldVal + keyCode);
				}

				setCurHasPoint(false);
				break;
			default:
				break;
		}
	};

	const clearResAndcalculatorStr = () => {
		setCalculatorStr('');
		setRes(null);
	}

	return (
		<div id='calculator' className='calculator'>
			<div className='bottom'>
				<div className='num'>
					{calculatorStr}
				</div>
				<div className='res'>
					{res}
				</div>
				<div className='actions'>
					<div className="list">
						<div className="item operator" onClick={() => clearResAndcalculatorStr()}>C</div>
						<div className="item operator" onClick={() => operatorAction('%', 'operator')}>%</div>
						<div className="item operator" onClick={() => operatorAction('Backspace', 'backspace')}>←</div>
						<div className="item operator" onClick={() => operatorAction('/', 'operator')}>➗</div>
					</div>
					<div className="list">
						<div className="item" onClick={() => operatorAction('7', 'num')}>7</div>
						<div className="item" onClick={() => operatorAction('8', 'num')}>8</div>
						<div className="item" onClick={() => operatorAction('9', 'num')}>9</div>
						<div className="item operator">*</div>
					</div>
					<div className="list">
						<div className="item" onClick={() => operatorAction('4', 'num')}>4</div>
						<div className="item" onClick={() => operatorAction('5', 'num')}>5</div>
						<div className="item" onClick={() => operatorAction('6', 'num')}>6</div>
						<div className="item operator" onClick={() => operatorAction('-', 'operator')}>—</div>
					</div>
					<div className="list">
						<div className="item" onClick={() => operatorAction('1', 'num')}>1</div>
						<div className="item" onClick={() => operatorAction('2', 'num')}>2</div>
						<div className="item" onClick={() => operatorAction('3', 'num')}>3</div>
						<div className="item operator" onClick={() => operatorAction('+', 'operator')}>+</div>
					</div>
					<div className="list">
						<div className="item" onClick={() => operatorAction('00', 'num')}>00</div>
						<div className="item" onClick={() => operatorAction('0', 'num')}>0</div>
						<div className="item" onClick={() => operatorAction('.', 'point')}>.</div>
						<div className="item equal" onClick={() => operatorAction('=', 'equal')}>=</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Calculator;