import React from 'react';

function Hex(props) {
	const { cnt = 0 } = props;
	
	return (
		<div className={`${cnt === 1 && 'yang'} hex-item`}>
			{
				cnt === 0 && <div className='yin'>
					<div className='yin-item'></div>
					<div className='yin-item'></div>
				</div>
			}
		</div>
	);
}

export default Hex;		