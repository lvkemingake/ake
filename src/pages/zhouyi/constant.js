export const HEX = [
	{ value: '111', lable: '乾' },
	{ value: '011', lable: '兑' },
	{ value: '101', lable: '离' },
	{ value: '001', lable: '震' },
	{ value: '110', lable: '巽' },
	{ value: '010', lable: '坎' },
	{ value: '100', lable: '艮' },
	{ value: '000', lable: '坤' },
];