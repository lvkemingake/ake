import React, { useState, useEffect } from 'react';
import { Button } from 'antd';
import './index.css';

import { HEX } from './constant';
import HexItem from './hex';

function ZhouYi(props) {

	const [randomList, setRandomList] = useState([]);
	const [hexagramList, setHexagramList] = useState([]); // 本卦
	const [inHexagramList, setInHexagramList] = useState([]); // 变卦
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		console.log(HEX);
	}, [])

	const generateNum = (time = 200) => {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				resolve(Math.round(Math.random() + 2));
			}, time)
		})
	}

	const startHex = async () => {
		setLoading(true);
		console.log('startHex---');
		// let first = await generateNum();
		// let second = await generateNum();
		// let third = await generateNum();
		let [first, second, third] = await Promise.all([generateNum(), generateNum(), generateNum()]);

		let temp = [...hexagramList], inTmep = [...inHexagramList], curSum = first + second + third;
		temp.unshift(curSum % 2);

		if (curSum === 6) {
			inTmep.unshift(1);
		} else if (curSum === 9) {
			inTmep.unshift(0);
		} else {
			inTmep.unshift(curSum % 2);
		}

		let tempRandom = [...randomList];
		tempRandom.unshift([first, second, third]);

		setHexagramList(temp);
		setInHexagramList(inTmep);
		setRandomList(tempRandom);
		setLoading(false);
	}

	const getHexLable = (a, b, c, d, e, f) => {
		let hex1 = HEX.find(item => {
			return item.value === '' + a + b + c
		});

		let hex2 = HEX.find(item => item.value === '' + d + e + f);

		if (hex1 && hex2) {
			return hex1.lable + hex2.lable;
		} else {
			return '';
		}
	}

	return (
		<div className='zhouyi'>
			<Button type='primary' onClick={startHex} disabled={hexagramList.length >= 6} loading={loading} >开始</Button>
			<div className='cnt'>
				{
					randomList.map(([first, second, third], index) => (
						<div key={index}>{6 - index}——{`${first}, ${second}, ${third}`}</div>
					))
				}
			</div>
			<div className='hexContent'>
				<div className='hex'>
					<div className='title'>本卦{getHexLable(...hexagramList)}</div>
					<div className='content'>
						{
							hexagramList.map((cnt, index) => (
								<HexItem key={'hex' + index} cnt={cnt} />
							))
						}
					</div>
				</div>
				<div className='in-hex'>
					<div className='title'>变卦{getHexLable(...inHexagramList)}</div>
					<div className='content'>
						{
							inHexagramList.map((cnt, index) => (
								<HexItem key={'inHex' + index} cnt={cnt} />
							))
						}
					</div>
				</div>
			</div>
		</div>
	);
}

export default ZhouYi;