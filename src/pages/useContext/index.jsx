import React, { useState, useContext, } from 'react';
import Context from '../../hooks/useContext';
import ContextCnt from '../../compoents/contextCnt';
import './index.css'

function UseContext(props) {
	const [cnt, setCnt] = useState(0);
	const [name, setName] = useState('name');

	console.log('render', name);
	return (
		<Context.Provider value={{
			cnt: cnt,
			setCnt: setCnt,
		}}>
			UseContext
			<div>cnt：{cnt}</div>
			<button onClick={() => {
				setName(pre => pre + 'sss')
				setCnt(pre => pre - 1)
			}}>-1</button>
			<Layout />
		</Context.Provider>
	);
}

function Layout(props) {
	return <div>
		<hr />
		<ContextCnt />
		<Content />
	</div>
}

function Content(props) {
	const { cnt, setCnt, } = useContext(Context);
	return <div>
		Content：{cnt}
		<button onClick={() => {
			setCnt(pre => pre + 1);
		}}>+1</button>
	</div>
}

export default UseContext;