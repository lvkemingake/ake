// 防抖
export function debounce(fn, wait) {
	let timer;
	return function () {
		clearTimeout(timer);
		timer = setTimeout(() => {
			fn.apply(this, arguments)   // 把参数传进去
		}, wait);
	}
}