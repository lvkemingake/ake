import { useNavigate } from 'react-router-dom';

export default function useRoute() {
	const navigate = useNavigate();

	return {
		navigate,
	}
}