import { useEffect, useState } from 'react';

const pageSize = 1;

const testTotal = 5;

const getMockData = ({ pageNum, pageSize, }) => {

	return new Promise((resolve, reject) => {
		setTimeout(() => {
			let list = []
			for (let i = 0; i < pageSize; i++) {
				list.push(Math.random());
			}

			resolve(list);
		}, 1000);
	});
}

const UseNextPage = (props) => {

	const [list, setList] = useState([]);
	const [pageNum, setPageNum] = useState(1);
	const [total, setTotal] = useState(0);
	
	useEffect(() => {
		getMockData({ pageNum, pageSize }).then(res => {
			setList((pre) => pre.concat(res));
			setTotal(testTotal);
		})
		console.log('UseNextPage-pageNum', pageNum);
	}, [pageNum])

	return {
		list,
		total,
		setPageNum,
	}
}

export default UseNextPage;