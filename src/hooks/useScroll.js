import { useEffect, useState, } from 'react';

function useScroll(props) {
	const { list } = props;

	const [curTab, setCurTab] = useState('');

	const [dataInView, setDataInView] = useState([]);

	useEffect(() => {
		setCurTab(list?.[0].value);
	}, [list])

	return {
		curTab,
		setCurTab,

		dataInView,
		setDataInView,
	}
}

export default useScroll;