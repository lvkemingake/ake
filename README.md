## ref的奇怪用法
```js
// 这样使用ref 每次更新页面的时候 dom都是最新的
const [cnt, setCnt] = useState(0);
const [dom, setDom] = useState(null);
console.log("RefDomTest", dom);
return <div ref={setDom}>RefDomTest --- {cnt}</div>;

// 可以结合下面的例子，能更好的理解上面为什么可以这么写
const ref = useRef();
return (
  <div id='ake'
    ref={(ele) => {
      // ele 就是 id为ake的div dom元素
      ref.current = ele;
    }}
  >
    ref
  </div>
);
```
